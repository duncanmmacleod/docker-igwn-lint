FROM igwn/base:el8

LABEL name="Markdown Lint"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# install available updates
RUN dnf -y update && dnf clean all

# install nodejs
RUN curl -fsSL https://rpm.nodesource.com/setup_lts.x | bash - && \
    dnf -y install nodejs && \
    dnf clean all

# install markdown lint
RUN npm install -g markdownlint-cli &&\
    npm cache clean --force
